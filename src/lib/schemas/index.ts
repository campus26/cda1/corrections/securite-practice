import { z } from 'zod';

export const loginSchema = z.object({
	username: z.string().min(1).max(256),
	password: z.string().min(8).max(256),
});

export const registerSchema = z.object({
	username: z.string().min(1).max(256),
	email: z.string().email(),
	password: z.string().min(1).max(256),
});

export const updateUserSchema = z.object({
	username: z.string().min(1).max(256).optional(),
	email: z.string().email().optional(),
	password: z.string().min(1).max(256).optional(),
});

export const safeReturnUser = z.object({
	username: z.string(),
	email: z.string().email(),
	id: z.number(),
});

export const welcomeNameSchema = z.string().min(1).max(256);
